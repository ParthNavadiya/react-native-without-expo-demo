import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
// import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const HomeScreen = () => {
  const [categoryList, setcategoryList] = useState([]);
  useEffect(() => {
    AsyncStorage.getItem('user_id').then((value) =>
      fetch('http://192.168.43.182:4000/category/list', {
        method: 'GET',
        headers: {
          token: value,
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          // setLoading(false);
          console.log('responseJson', responseJson)
          if (responseJson.status === 'success') {
            setcategoryList(responseJson.data)
          } else {
            setErrortext(responseJson.msg);
            console.log('Please check your email id or password');
          }
        })
        .catch((error) => {
          // setLoading(false);
          console.error(error);
        }));
  }, [])

  const HomeScreen = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Home!</Text>
      </View>
    );
  }

  const SettingsScreen = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }

  // const Tab = createBottomTabNavigator();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {/* <Tab.Navigator
        initialRouteName="Marvel"
        tabBarOptions={{
          activeTintColor: "#e91e63",
          inactiveTintColor: "#fff",
          labelStyle: { fontSize: 14, fontWeight: "bold" },
          style: { backgroundColor: "#633689" },
        }}
      >
        <Tab.Screen
          name="Marvel"
          component={HomeScreen}
          options={{ tabBarLabel: "MC" }}
        />
        <Tab.Screen
          name="DC"
          component={SettingsScreen}
          options={{ tabBarLabel: "DC" }}
        />
      </Tab.Navigator> */}
      <View style={{ flex: 1, padding: 16 }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 20,
              textAlign: 'center',
              marginBottom: 16,
            }}>
            My Galary
            {'\n\n'}
            Home Screen
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;