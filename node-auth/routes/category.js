const express = require("express");
const { check, validationResult } = require("express-validator/check");
const auth = require("../middleware/auth");
const router = express.Router();

const category = require("../model/Category");

router.get("/list", auth, async (req, res) => {
  try {
    const categorylist = await category.find();
    res.json({ data: categorylist, status: 'success' });
  } catch (e) {
    res.send({ message: "Error in Fetching categorylist" });
  }
});

module.exports = router;
