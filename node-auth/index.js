const express = require("express");
const bodyParser = require("body-parser");
const user = require("./routes/user");
const category = require("./routes/category");
const InitiateMongoServer = require("./config/db");
const cors = require("cors");

InitiateMongoServer();

const app = express();

// PORT
const PORT = process.env.PORT || 4000;

app.use(bodyParser.json());
app.use(cors());

app.use("/user", user);
app.use("/category", category);

app.listen(PORT, (req, res) => {
  console.log(`Server Started at PORT ${PORT}`);
});
