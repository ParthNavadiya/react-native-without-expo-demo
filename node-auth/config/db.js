const mongoose = require("mongoose");

const MONGOURI = "mongodb+srv://parth:test%40123@cluster0.s6mfi.mongodb.net/native-application";

const InitiateMongoServer = async () => {
  try {
    await mongoose.connect(MONGOURI, {
      useNewUrlParser: true
    });
    console.log("Connected to DB !!");
  } catch (e) {
    console.log(e);
    throw e;
  }
};

module.exports = InitiateMongoServer;